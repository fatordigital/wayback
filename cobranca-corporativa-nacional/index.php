<?php
error_reporting(0);
@ini_set('display_errors', 0);

session_start();


if (strstr($_SERVER['HTTP_HOST'], '.dev')) {
    $status = 'dev';
} else {
    $status = 'prod';
    if ((strpos($_SERVER['HTTP_HOST'],'www.')===false)) {
        header('Location: http://www.' . $_SERVER['HTTP_HOST'].'/cobranca-corporativa-nacional'); exit();
    }
}

date_default_timezone_set('America/Sao_Paulo');


require '../functions/fator_digital_functions.php';

require_once("../phpmailer/class.phpmailer.php");

define('GUSER', 'infowayback@gmail.com'); // <-- Insira aqui o seu GMail
define('GPWD', 'wayback3');   // <-- Insira aqui a senha do seu GMail


if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    if (!isset($_POST['fd_key']) || $_POST['fd_key'] != '') {
        $_SESSION['json'] = json_encode(array(
            'error' => true,
            'msg' => 'Não foi possível continuar',
            'data' => $_POST
        ));
        back();
    }

    //$para = "comercial@wayback.com.br";
    $para = "comercial@wayback.com.br";
//    $para = "andrew.rodrigues@fatordigital.com.br";
    $post = array_map('trim', $_POST);

    
    if (!fator_digital_key()) {
        $_SESSION['json'] = json_encode(array(
            'error' => true,
            'msg' => 'Não foi possível enviar seu contato, atualize a página e tente novamente.',
            'data' => $_POST
        ));
    } else {
        $continue = false;
        if (has_empty('tipo') && !in_array(get('tipo'), array('conhecer-nossos-servicos', 'duvidas-e-sugestoes'))) {
            $_SESSION['json'] = json_encode(array(
                'error' => true,
                'msg' => 'Não foi possível enviar seu contato, atualize a página e tente novamente.',
                'data' => $_POST
            ));
        } else {

            if (get('tipo') == 'conhecer-nossos-servicos') {

                $assunto = 'Nacional - Conhecer nossos serviços - ContatoWayBackCollections';

                if (has_empty('name') || has_empty('email') || has_empty('assunto') || has_empty('telephone') || has_empty('empresa')) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Todos os campos são obrigatórios.',
                        'data' => $_POST
                    ));
                    $continue = false;
                }
                $continue = true;
            } else if (get('tipo') == 'duvidas-e-sugestoes') {

                $assunto = 'Nacional - Dúvidas ou Sugestões - ContatoWayBackCollections';

                if (has_empty('name') || has_empty('telephone') || has_empty('email') || has_empty('assunto') || has_empty('message')) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Preencha os campos obrigatórios.',
                        'data' => $_POST
                    ));
                    $continue = false;
                }
                $continue = true;
            }



            if ($continue) {

                $mensagem = "<b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Dados para contato</span></b>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>Empresa: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('empresa') . "</span>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>Nome: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('name') . "</span>";
                $mensagem .= "<BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>E-mail: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('email') . "</span>";
                $mensagem .= "<BR><BR><p class='MsoNormal'><b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Assunto: </span></b>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('assunto') . "</span>";
                $mensagem .= "<BR><BR><p class='MsoNormal'><b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Telefone: </span></b>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('telephone') . "</span>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'><b>Mensagem: </b></span>";
                $mensagem .= "<BR><span style='font-family: Calibri,sans-serif'>" . get('message') . "</span><BR><BR>";
                $mensagem .= "<br><span style='font-family: Calibri,sans-serif; color: #843C0C'><b><font size='2'>Essa mensagem foi enviada pela pagina de Contato www.wayback.com.br/cobranca-corporativa-nacional<br></font>";

                if (smtpmailer($para, get('email'), get('name'), $assunto, $mensagem)) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => false,
                        'msg' => 'Contato enviado com sucesso, obrigado!',
                        'data' => $_POST,
                        'goal' => '/goal/nacional/'. get('tipo')
                    ));
                    back();
                } else {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Não foi possível receber seu contato, tente novamente, caso o erro persista, tente novamente mais tarde.',
                        'data' => $_POST
                    ));
                    back();
                }

            } else {
                back();
            }
        }
    }
}

?>

<!DOCTYPE html>
<!-- xLander - Premium Flexible Landing Page Template design by DSA79 (http://www.dsathemes.com) -->
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="pt-br"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="pt-br"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="pt-br"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="pt-br"><!--<![endif]-->

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <title>Way Back - Cobrança Corporativa Nacional</title>


    <meta name="keywords"
          content="way back, mooca, cobranca, cobrança, cobranca corporativa, cobrança corporativa, recuperacao de credito, recuperação de crédito, recuperacao de creditos, recuperação de créditos, terceirizacao de negocios, terceirização de negócios, bpo, empresa de cobranca, empresa de cobrança,"/>

    <meta name="author" content="Thiago Jambor">
    <meta name="robots" content="index"/>

    <meta name="description"
          content="Somos especialistas em Cobrança Corporativa Nacional, Internacional e Terceirização de Processos de Negócios. Fale Conosco: 11 2148-9100 | contato@wayback.com.br"/>


    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="http://www.wayback.com.br">
    <meta property="og:title" content="Way Back - Cobrança Corporativa e Terceirização de Negócios">
    <meta property="og:site_name" content="Way Back">
    <meta property="og:description" content="Somos especialistas em Cobrança Corporativa Nacional, Internacional e Terceirização de Processos de Negócios. Fale Conosco: 11 2148-9100 | contato@wayback.com.br">

    <meta property="og:image" content="http://www.wayback.com.br/images/wayback-og.png">

    <!-- Libs CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">

    <!-- On Scroll Animations -->
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Template CSS -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/blue.css" rel="stylesheet">


    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="../wayback-icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../wayback-icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../wayback-icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../wayback-icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../wayback-icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../wayback-icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../wayback-icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../wayback-icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../wayback-icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../wayback-icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../wayback-icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../wayback-icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../wayback-icon/favicon-16x16.png">
    <link rel="manifest" href="../wayback-icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../wayback-icon/ms-icon-144x144.png">



    <!-- Google Fonts -->
    <link
        href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,800italic,800,700italic,700,600italic,600,400italic,300'
        rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="../dist/sweetalert.css">



    <link rel="stylesheet" href="../css/custom.css" />


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102512770-1', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PW84RV8');</script>
    <!-- End Google Tag Manager -->

</head>


<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PW84RV8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



<!-- HEADER
============================================= -->
<header id="header">
    <div class="navbar navbar-fixed-top">
        <div class="container">


            <!-- Navigation Bar -->
            <div class="navbar-header">

                <!-- Responsive Menu Button -->
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse"
                        data-target="#navigation-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Logo Image -->
                <a class="navbar-brand" href="#"><img src="img/logo.png" alt="Way Back - Logo" role="banner" /></a>

            </div>    <!-- End Navigation Bar -->


            <!-- Navigation Menu -->
            <nav id="navigation-menu" class="collapse navbar-collapse" role="navigation">
                <ul class="nav navbar-nav navbar-right">

                    <li><a id="GoToHome" class="selected-nav" href="#intro">Home</a></li>
                    <li><a id="GoToCobranca" href="#cobranca">Quem Somos</a></li>
                    <li><a id="GoToDiferenciais" href="#diferenciais">Diferenciais</a></li>


                    <li><a id="GoToFaq" href="#faq">FAQ</a></li>

                    <li><a id="GoToContato" href="#contato">Contato</a></li>
                </ul>
            </nav>  <!-- End Navigation Menu -->


        </div>     <!-- End container -->
    </div>      <!-- End navbar fixed top  -->
</header>    <!-- END HEADER -->


<!-- CONTENT WRAPPER
============================================= -->
<div id="content_wrapper">


    <!-- INTRO
    ============================================= -->
    <section id="intro">
        <div class="overlay">
            <div class="container">
                <div id="intro_content" class="row">


                    <!-- INTRO TEXT -->
                    <div class="col-md-7">

                        <div class="intro_text">

                            <br><br><br><br><br><br><br>


                            <h1 class="h4a">Cobrança<br>Corporativa Nacional</h1>

                            <br><br>

                            <p>Especialista em Recuperação de Créditos B2B, B2C, Ticket Alto<br>do amigável ao judicial.
                            </p>

                        </div>


                    </div>    <!-- END TEXT -->


                    <!-- INTRO REGISTER FORM -->
                    <div id="intro_form" class="col-md-5 form_register text-center">

                        <!-- Register Form -->
                        <form action="" name="contactform" class="row" method="post">

                            <input type="hidden" name="tipo" value="conhecer-nossos-servicos" />
                            
                            <div class="fd_key">
                                <input type="text" name="fd_key" value="" />
                            </div>

                            <h4>Quer conhecer nossos serviços? Entre em Contato</h4>

                            <div id="input_name" class="col-md-12">
                                <input class="form-control" type="text" name="name" placeholder="Nome:">
                            </div>

                            <div id="input_email" class="col-md-12">
                                <input class="form-control" type="text" name="email" placeholder="Email:">
                            </div>

                            <div id="input_phone" class="col-md-12">
                                <input class="form-control phone-mask" type="text" name="telephone" pattern="[\(]\d{2}[\)] (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})" placeholder="Telefone:">
                            </div>


                            <div id="input_phone" class="col-md-12">
                                <textarea name="message" class="form-control" placeholder="Mensagem:"></textarea>
                            </div>

                            <p class="hide">Entre em contato com nosso Time Comercial e tire suas dúvidas sobre nossos serviços.</p>

                            <!-- Submit Button -->
                            <div id="form_register_btn" class="text-center">
                                <input class="btn btn-theme" type="submit" value="Enviar Mensagem">
                            </div>

                            <p class="phone_number">
                                Ou entre em contato por telefone
                                <br />
                                <?php if(!isset($_GET['fd'])): ?>
                                <strong><small>+55 11</small> 2148-9100</strong>
                                <?php else: ?>
                                <strong><small>+55 11</small> 3230-5135</strong>
                                <?php endIf; ?>
                            </p>

                        </form>    <!-- End Register Form -->

                    </div>    <!-- END INTRO REGISTER FORM -->


                </div>     <!-- End Intro Content -->
            </div>    <!-- End container -->
        </div>       <!-- End overlay-->
    </section>    <!-- END INTRO -->


    <!-- ABOUT
    ============================================= -->
    <section id="cobranca">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Cobrança Corporativa Nacional</h3>
                    <h2>Way Back</h2>
                </div>
            </div>


            <!-- TABS HOLDER -->
            <div id="tabs-holder" class="row">


                <!-- TABS -->
                <div class="col-md-7">


                    <!-- Nav Tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active animated" data-animation="bounceIn" data-animation-delay="300"><a
                                href="#tab_1" role="tab" data-toggle="tab">Quem Somos</a></li>
                        <li class=" animated" data-animation="bounceIn" data-animation-delay="400"><a href="#tab_2"
                                                                                                      role="tab"
                                                                                                      data-toggle="tab">Nossos
                                Serviços</a></li>
                    </ul>


                    <!-- Tab Panes -->
                    <div class="tab-content">


                        <!-- TAB #1 -->
                        <div id="tab_1" class="tab-pane active">

                            <!-- Tab #1 Description -->
                            <p>A Way Back Collections | BPO, desde 1991, é especialista em Cobrança Corporativa
                                Nacional, Internacional e na melhoria de processos dentro do conceito de BPO (Business
                                Process Outsourcing).
                            </p>
                            <br>
                            <p>Estamos sempre antenados em inovações e melhores práticas de mercado, com o compromisso
                                único de proporcionar o encantamento de nossos clientes e parceiros. </p>

                            <br>
                            <p>Somos Membro <span style="background-color: #bfe9f7">acionista exclusivo no Brasil</span>
                                da TCM Group Global Debt Collection (maior conglomerado de Crédito & Cobrança do mundo)
                                e também filiada ao Instituto GEOC, ACA, IACC e LatinCob.</p>

                            <!-- Tab #2 Buttons -->
                            <a href="http://www.wayback.com.br" target="_blank" class="btn btn-theme animated"
                               data-animation="bounceIn" data-animation-delay="300">Visite nosso Site Institucional</a>


                        </div>    <!-- END TAB #1 -->


                        <!-- TAB #2 -->
                        <div id="tab_2" class="tab-pane">

                            <!-- Tab #2 Description -->
                            <p>São serviços e profissionais <span
                                    style="background-color: #bfe9f7">especializados</span> em Recuperação de Créditos
                                B2B, B2C, Ticket Alto. Atuamos em toda a régua de cobrança, de Ações Preventivas a
                                Resolutiva em âmbito amigável e judicial.</p>

                            <br>

                            <p>
                                <span style="background-color: #bfe9f7"><b>Ações Preventivas:</b></span> Torre de
                                Crédito (Antes do Faturamento), Gestão Preventiva de Ativos (Antes do Vencimento) e
                                Recuperação Consultiva (Antes de 30 dias).
                                <br><br>
                                <span style="background-color: #bfe9f7"><b>Ações Resolutivas:</b></span> Amigável (Pós
                                30 dias), Pré-Judicial, Judicial e Pós-Judicial.

                            </p>


                            <!-- Tab #2 Buttons -->
                            <a href="#contact" class="btn btn-theme animated" data-animation="bounceIn"
                               data-animation-delay="300">Quer entender melhor? Solicite nossa Régua de Serviços</a>


                        </div>    <!-- END TAB #2 -->


                    </div>    <!-- End Tab Panes -->


                </div>    <!-- END TABS -->


                <!-- TABS HOLDER IMAGE -->
                <div class="col-md-5 text-center animated" data-animation="fadeInRight" data-animation-delay="600">
                    <img class="img-responsive" src="img/thumbs/about-image.jpg" title="Quem Somos | Nossos Serviços - Way Back"/>
                </div>


            </div>    <!-- END TABS HOLDER -->


        </div>      <!-- End container -->
    </section>    <!-- END ABOUT -->


    <!-- SKILLS
    ============================================= -->
    <section id="skills">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h2>Segmentos de Atuação</h2>
                </div>
            </div>


            <!-- SKILLS HOLDER -->
            <div class="row">


                <!-- WEB DESIGN -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="300">

                    <div class="chart">
                        <div class="percentage" data-percent="33">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Indústrias</h4>

                </div>

                <!-- WEB DESIGN -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="300">

                    <div class="chart">
                        <div class="percentage" data-percent="21">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Comércios</h4>

                </div>


                <!-- WEB DESIGN -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="300">

                    <div class="chart">
                        <div class="percentage" data-percent="4">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Internacional</h4>

                </div>


                <!-- WORDPRESS -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="500">

                    <div class="chart">
                        <div class="percentage" data-percent="2">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Outros</h4>

                </div>


                <!-- JQUERY -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="700">

                    <div class="chart">
                        <div class="percentage" data-percent="24">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Serviços</h4>

                </div>


                <!-- HTML/CSS -->
                <div class="col-xs-6 col-sm-2 text-center animated" data-animation="bounceIn"
                     data-animation-delay="900">

                    <div class="chart">
                        <div class="percentage" data-percent="16">
                            <span class="percent"></span>
                        </div>
                    </div>

                    <h4>Bancos</h4>

                </div>


            </div>    <!-- END SKILLS HOLDER -->


        </div>      <!-- End container -->
    </section>    <!-- END SKILLS -->


    <!-- SERVICES
    ============================================= -->
    <section id="diferenciais">
        <div class="container">


            <!-- SERVICE IMAGE -->
            <div class="row">
                <div id="services_image" class="col-md-12 text-center animated" data-animation="fadeInUp"
                     data-animation-delay="300">
                    <img class="img-responsive" src="img/thumbs/features-image.png" alt="Quais são nossos diferenciais - Way Back"/>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="300">
                        <h4><i class="fa fa-cogs"></i>750 Posições de Negociações</h4>
                        <p>Células de negociações ativas e receptivas, estruturadas de acordo com o ramo de atividade do
                            contratante e por faixa de valores. </p>
                    </div>


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="500">
                        <h4><i class="fa fa-cogs"></i>Suporte Jurídico</h4>
                        <p>Contamos com 255 Escritórios de Advocacia parceiros e criamos um departamento especializado
                            em Ação Pré-Judicial, Judicial e Gestão Pós-Judicial - NAJUD (Núcleo de Acordos
                            Judiciais). </p>
                    </div>


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="500">
                        <h4><i class="fa fa-cogs"></i>Sistema Way Back</h4>
                        <p>Sistema inovador com discagem automática, controle de estatísticas, monitoramento e
                            relatórios online, URA, ligações gravadas, registro de chamadas e contatos telefônicos. </p>
                    </div>


                </div>


                <div class="col-md-6">


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="300">
                        <h4><i class="fa fa-cogs"></i>Ferramentas Otimizadas</h4>
                        <p>Higienização, enriquecimento e otimização nas bases B2B, B2C e BPO. Sistemas próprios e
                            licenciados. </p>
                    </div>


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="500">
                        <h4><i class="fa fa-cogs"></i>Gestão de Qualidade</h4>
                        <p>Negociadores monitorados constantemente e que passam por intenso treinamento sobre a postura
                            da empresa, qualidade na negociação e no atendimento a clientes. </p>
                    </div>


                    <!-- SERVICE BOX -->
                    <div class="service-box animated" data-animation="fadeInUp" data-animation-delay="500">
                        <h4><i class="fa fa-cogs"></i>CERTA</h4>
                        <p>Célula Especializada em Recuperação de Ticket Alto - Tratamento diferenciado na
                            operacionalização da Cobrança de Títulos de valores expressivos. </p>
                    </div>


                </div>


            </div>


            <!-- SERVICE BUTTONS -->
            <div class="row">
                <div id="service-buttons" class="col-md-12 text-center">
                    <a href="#contact" class="btn btn-theme animated" data-animation="bounceIn"
                       data-animation-delay="600">Dúvidas? Entre em contato com nosso Time Comercial</a>

                </div>
            </div>


        </div>        <!-- End container -->
    </section>    <!-- END SERVICES -->


    <!-- PROMO LINE
============================================= -->
    <div id="promo_line">
        <div class="container">
            <div class="row">


                <!-- PROMO LINE CONTENT -->
                <div class="col-md-12 text-center">

                    <h2>Soluções Inteligentes</h2>
                    <br>
                    <p>Conheça mais sobre nossas diversas Soluções Inteligentes, além, da Cobrança Corporativa
                        Nacional </p>
                    <br><br>
                    <a href="http://www.wayback.com.br/internacional" target="_blank" class="btn btn-theme">Cobrança
                        Corporativa Internacional</a>
                    <a href="http://www.wayback.com.br/bpo" target="_blank" class="btn btn-theme2">Terceirização de
                        Processos de Negócios</a>

                </div>


            </div>     <!-- End row -->
        </div>      <!-- End container -->
    </div>        <!-- END PROMO LINE -->


    <!-- ABOUT-1
    ============================================= -->
    <section id="faq">
        <div class="container">


            <!-- ACCORDION HOLDER -->
            <div id="accordion-holder" class="row">


                <!-- ACCORDION HOLDER IMAGE -->
                <div class="col-md-6 text-center animated" data-animation="fadeInLeft" data-animation-delay="600">
                    <img class="img-responsive" src="img/thumbs/tablet.png" alt="Orientações gerais - Way Back" />
                </div>


                <!--  ACCORDION -->
                <div class="col-md-6">
                    <div class="panel-group" id="accordion">


                        <!-- PANEL #1 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="300">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h5 class="panel-title"><a data-toggle="collapse" class="panel-toggle active"
                                                           data-parent="#accordion" href="#collapseOne"><i
                                            class="fa fa-check"></i>O que é cobrança corporativa ?</a></h5>
                            </div>

                            <!-- Panel Content -->
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Cobrança corporativa compreende ações de recuperação de créditos vencidos e não
                                    pagos em que o credor e devedor são pessoas jurídicas (B2B).
                                </div>
                            </div>

                        </div>    <!-- END PANEL #1 -->


                        <!-- PANEL #2 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="500">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseTwo"><i
                                            class="fa fa-check"></i>Qual o conceito de B2B ?</a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    São negócios realizados (de produtos ou serviços ) entre empresas, pessoas
                                    jurídicas.

                                </div>
                            </div>

                        </div>    <!-- END PANEL #2 -->


                        <!-- PANEL #3 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="700">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseThree"><i
                                            class="fa fa-check"></i>Por que terceirizar a cobrança ?</a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Porque uma empresa especialista possui recursos, pessoas e gestão necessários para
                                    promover a recuperação dos créditos com preservação da relação comercial, deixando o
                                    credor focado no seu negócio principal.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #3 -->


                        <!-- PANEL #4 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="900">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseFour"><i
                                            class="fa fa-check"></i>Em que momento terceirizar ?</a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    A terceirização da carteira de cobranças, de um modo geral, obedece a política e
                                    cultura de cada empresa, mas como especialistas, propomos que a transferência dos
                                    títulos seja realizada logo nas primeiras faixas de atraso (0-30/31-60/61-90).
                                </div>
                            </div>

                        </div>    <!-- END PANEL #4 -->


                        <!-- PANEL #5 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="700">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseFive"><i
                                            class="fa fa-check"></i>Quais serão as bases da contratação de uma empresa
                                        de cobrança ?</a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Em contrato, define-se todas as diretrizes como: os parâmetros de negociação, tempo
                                    de permanência dos títulos (na empresa de cobrança) forma de recebimento do crédito,
                                    remuneração e demais disposições importantes para a execução do trabalho.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #3 -->


                    </div>
                </div>    <!--  END ACCORDION -->


            </div>    <!-- END ACCORDION HOLDER -->


        </div>      <!-- End container -->
    </section>    <!-- END ABOUT-1 -->


    <!-- CONTACT-INFO
    ============================================= -->
    <section id="contato">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Fale Conosco</h3>
                    <h2>Entre em contato com nosso Time Comercial</h2>
                </div>
            </div>


            <div class="row">


                <!-- Office Location -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-map-marker"></i>
                    <h4 class="contact_title">Localização</h4>
                    <p class="contact_description">Av. Paes de Barros, 1162 - Mooca<br>03114-000 - São Paulo/SP</p>
                </div>


                <!-- Call Us -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-phone"></i>
                    <h4 class="contact_title">Telefone</h4>

                    <?php if(!isset($_GET['fd'])): ?>
                    <p class="contact_description"> +55 11 2148-9100</p>
                    <?php else: ?>
                    <p class="contact_description"> +55 11 3230-5135</p>
                    <?php endIf; ?>
                </div>


                <!-- Email Address -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-envelope-o"></i>
                    <h4 class="contact_title">Email</h4>
                    <a href="mailto:comercial@wayback.com.br">comercial@wayback.com.br</a>
                </div>


                <!-- Working Hours -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-clock-o"></i>
                    <h4 class="contact_title">Atendimento</h4>
                    <p class="contact_description">Seg. à Qui. - 08h às 18h<br>Sex. - 08h às 17h</p>
                </div>


            </div>    <!-- End row -->


        </div>             <!-- End container -->
    </section>       <!-- END CONTACT-INFO -->


    <!-- CONTACTS
     ============================================= -->
    <section id="contact">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Dúvidas ou Sugestões?</h3>
                    <h2>Deixe sua mensagem que retornaremos o contato</h2>
                </div>
            </div>


            <!-- CONTACT FORM -->
            <div class="row">
                <div class="col-sm-12">

                    <form id="contact-form" action="" name="contactform" class="row" method="post">

                        <input type="hidden" name="tipo" value="duvidas-e-sugestoes" />

                        <div class="fd_key">
                            <input type="text" name="fd_key" value="" />
                        </div>

                        <div id="input_name" class="col-md-6">
                            <input type="text" name="name" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Nome:">
                        </div>

                        <div id="input_email" class="col-md-6">
                            <input type="text" name="empresa" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Empresa:">
                        </div>


                        <div id="input_name" class="col-md-6">
                            <input type="text" name="telephone" pattern="[\(]\d{2}[\)] (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})" class="form-control text-white phone-mask" placeholder="Telefone:">
                        </div>

                        <div id="input_email" class="col-md-6">
                            <input type="text" name="email" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Email:">
                        </div>


                        <div id="input_subject" class="col-md-12">
                            <input type="text" name="assunto" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Assunto:">
                        </div>

                        <div id="input_message" class="col-md-12">
                            <textarea class="form-control triggerAnimation text-white animated" data-animate="bounceIn"
                                      name="message" rows="6" placeholder="Mensagem:"></textarea>
                        </div>

                        <!-- Submit Button -->
                        <div id="form_btn" class="col-md-12">
                            <div class="text-center">
                                <input type="submit" value="ENVIAR MENSAGEM" class="btn btn-theme triggerAnimation animated" data-animate="bounceIn">
                            </div>
                        </div>

                    </form>

                </div>
            </div>       <!-- END CONTACT FORM -->


        </div>       <!-- End container -->
    </section>     <!-- END CONTACTS -->


    <!-- FOOTER
    ============================================= -->
    <footer id="footer">
        <div class="container">


            <!-- FOOTER COPYRIGHT -->
            <div class="row">
                <div id="footer_copyright" class="col-sm-12 text-center">
                    <p>Copyright Way Back © 2017. All Rights Reserved.</p>
                </div>
            </div>


            <!-- FOOTER SOCIALS -->
            <div class="row">

                <div id="footer_socials" class="col-sm-12 text-center">

                    <div id="contact_icons">
                        <ul class="contact-socials clearfix">
                            <li><a class="foo_social ico-facebook" href="https://www.facebook.com/WayBack.BPO"
                                   target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="foo_social ico-linkedin"
                                   href="https://www.linkedin.com/company/way-back-servi-os-de-cobran-a-ltda"
                                   target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a class="foo_social ico-twitter" href="https://twitter.com/waybackbr"
                                   target="_blank"><i class="fa fa-twitter"></i></a></li>

                            <li><a class="foo_social ico-youtube" href="https://www.youtube.com/user/WayBackBR"
                                   target="_blank"><i class="fa fa-youtube"></i></a></li>

                            <!--
                                <li><a class="foo_social ico-dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a class="foo_social ico-pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="foo_social ico-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="foo_social ico-digg" href="#"><i class="fa fa-digg"></i></a></li>
                                <li><a class="foo_social ico-deviantart" href="#"><i class="fa fa-deviantart"></i></a></li>
                                <li><a class="foo_social ico-envelope" href="#"><i class="fa fa-envelope-square"></i></a></li>
                                <li><a class="foo_social ico-delicious" href="#"><i class="fa fa-delicious"></i></a></li>
                                <li><a class="foo_social ico-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="foo_social ico-dropbox" href="#"><i class="fa fa-dropbox"></i></a></li>
                                <li><a class="foo_social ico-skype" href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a class="foo_social ico-youtube" href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a class="foo_social ico-tumblr" href="#"><i class="fa fa-tumblr"></i></a></li>
                                <li><a class="foo_social ico-vimeo" href="#"><i class="fa fa-vimeo-square"></i></a></li>
                                <li><a class="foo_social ico-flickr" href="#"><i class="fa fa-flickr"></i></a></li>
                                <li><a class="foo_social ico-github" href="#"><i class="fa fa-github-alt"></i></a></li>
                                <li><a class="foo_social ico-renren" href="#"><i class="fa fa-renren"></i></a></li>
                                <li><a class="foo_social ico-vk" href="#"><i class="fa fa-vk"></i></a></li>
                                <li><a class="foo_social ico-xing" href="#"><i class="fa fa-xing"></i></a></li>
                                <li><a class="foo_social ico-weibo" href="#"><i class="fa fa-weibo"></i></a></li>
                                <li><a class="foo_social ico-rss" href="#"><i class="fa fa-rss"></i></a></li>
                            -->

                        </ul>
                    </div>

                </div>

            </div>    <!-- END FOOTER SOCIALS -->


        </div>      <!-- End container -->
    </footer>    <!-- END FOOTER -->


</div>    <!-- END CONTENT WRAPPER -->


<!-- EXTERNAL SCRIPTS
============================================= -->
<script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/modernizr.custom.js" type="text/javascript"></script>
<script src="js/jquery.mask.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.js" type="text/javascript"></script>
<script src="js/retina.js" type="text/javascript"></script>
<script src="js/jquery.stellar.min.js" type="text/javascript"></script>
<script defer src="js/jquery.flexslider.js" type="text/javascript"></script>
<script src="js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script defer src="js/count-to.js"></script>
<script defer src="js/jquery.appear.js"></script>
<script src="js/jquery.mixitup.js" type="text/javascript"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script src="js/jquery.easypiechart.min.js"></script>
<script defer src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/waypoints.min.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>


<!-- Google Map -->
<!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
<!--<script src="js/jquery.gmap3.min.js"></script>-->


<script src="../dist/sweetalert-dev.js"></script>
<script language="javascript">
    var win = null;
    function NovaJanela(pagina, nome, w, h, scroll) {
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',resizable'
        win = window.open(pagina, nome, settings);
    }

    function JSalert($title, $text, $status) {
        swal($title, $text, $status);
    }
</script>

<script type="text/javascript">
    <?php
    if (session_has('json')) {
        $json = json_decode(session('json'));
        if ($json->error) {
            echo 'JSalert("Ops", "'.$json->msg.'", "error");';
        } else {
            echo 'JSalert("Contato", "'.$json->msg.'", "success");';
            echo 'ga("send", "pageview", "'.$json->goal.'");';
        }
        session_destroy();
    }
    ?>
</script>

<!-- Google Map Init-->
<!--<script type="text/javascript">-->
<!--    jQuery(function ($) {-->
<!--        $('#map_canvas').gmap3({-->
<!--            marker: {-->
<!--                address: '-23.56472045,-46.59618393',-->
<!--                options: {icon: "img/icons/map-marker.png"}-->
<!--            },-->
<!--            map: {-->
<!--                options: {-->
<!--                    zoom: 16,-->
<!--                    scrollwheel: false,-->
<!--                    streetViewControl: true,-->
<!--                    styles: [{-->
<!--                        featureType: "all",-->
<!--                        stylers: [-->
<!--                            {saturation: -100}-->
<!--                        ]-->
<!--                    }, {-->
<!--                        featureType: "road.arterial",-->
<!--                        elementType: "geometry",-->
<!--                        stylers: [-->
<!--                            {hue: "#000000"},-->
<!--                            {saturation: -100}-->
<!--                        ]-->
<!--                    }-->
<!--                    ],-->
<!--                }-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->




<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->


<script type="text/javascript">
    var _fdq = _fdq || [];
    _fdq._setAccount = 'FD_0011';

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.fatordigital.com.br/metriks.min.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

<script type="text/javascript" src="https://s3-sa-east-1.amazonaws.com/phonetrack-static/59c33016884a62116be975a9bb8257e3.js" id="script-pht-phone" data-cookiedays="5"></script>

</body>

</html>