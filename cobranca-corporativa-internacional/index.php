<?php
error_reporting(0);
@ini_set('display_errors', 0);

session_start();


if (strstr($_SERVER['HTTP_HOST'], '.dev')) {
    $status = 'dev';
} else {
    $status = 'prod';
    if ((strpos($_SERVER['HTTP_HOST'],'www.')===false)) {
        header('Location: http://www.' . $_SERVER['HTTP_HOST'].'/cobranca-corporativa-internacional'); exit();
    }
}

date_default_timezone_set('America/Sao_Paulo');


require '../functions/fator_digital_functions.php';

require_once("../phpmailer/class.phpmailer.php");

define('GUSER', 'infowayback@gmail.com'); // <-- Insira aqui o seu GMail
define('GPWD', 'wayback3');   // <-- Insira aqui a senha do seu GMail


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!isset($_POST['fd_key']) || $_POST['fd_key'] != '') {
        $_SESSION['json'] = json_encode(array(
            'error' => true,
            'msg' => 'Não foi possível continuar',
            'data' => $_POST
        ));
        back();
    }

    //$para = "international@wayback.com.br";
    $para = "comercial@wayback.com.br";
//    $para = "andrew.rodrigues@fatordigital.com.br";
    $post = array_map('trim', $_POST);

    if (!fator_digital_key()) {
        $_SESSION['json'] = json_encode(array(
            'error' => true,
            'msg' => 'Não foi possível enviar seu contato, atualize a página e tente novamente.',
            'data' => $_POST
        ));
    } else {
        $continue = false;
        if (has_empty('tipo') && !in_array(get('tipo'), array('conhecer-nossos-servicos', 'duvidas-e-sugestoes'))) {
            $_SESSION['json'] = json_encode(array(
                'error' => true,
                'msg' => 'Não foi possível enviar seu contato, atualize a página e tente novamente.',
                'data' => $_POST
            ));
        } else {

            if (get('tipo') == 'conhecer-nossos-servicos') {

                $assunto = 'Internacional - Conhecer nossos serviços - Way Back BRA';

                if (has_empty('name') || has_empty('email') || has_empty('assunto') || has_empty('telephone') || has_empty('empresa')) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Todos os campos são obrigatórios.',
                        'data' => $_POST
                    ));
                    $continue = false;
                }
                $continue = true;
            } else if (get('tipo') == 'duvidas-e-sugestoes') {

                $assunto = 'Internacional - Dúvidas ou Sugestões - Way Back BRA';

                if (has_empty('name') || has_empty('telephone') || has_empty('email') || has_empty('assunto') || has_empty('message')) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Preencha os campos obrigatórios.',
                        'data' => $_POST
                    ));
                    $continue = false;
                }
                $continue = true;
            }

            if ($continue) {

                $mensagem = "<b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Dados para contato</span></b>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>Empresa: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('empresa') . "</span>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>Nome: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('name') . "</span>";
                $mensagem .= "<BR><span style='font-family: Calibri,sans-serif; color: #843C0C'>E-mail: </span>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('email') . "</span>";
                $mensagem .= "<BR><BR><p class='MsoNormal'><b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Assunto: </span></b>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('assunto') . "</span>";
                $mensagem .= "<BR><BR><p class='MsoNormal'><b><span style='font-family: Calibri,sans-serif; color: #843C0C'>Telefone: </span></b>";
                $mensagem .= "<span style='font-family: Calibri,sans-serif'>" . get('telephone') . "</span>";
                $mensagem .= "<BR><BR><span style='font-family: Calibri,sans-serif; color: #843C0C'><b>Mensagem: </b></span>";
                $mensagem .= "<BR><span style='font-family: Calibri,sans-serif'>" . get('message') . "</span><BR><BR>";
                $mensagem .= "<br><span style='font-family: Calibri,sans-serif; color: #843C0C'><b><font size='2'>Essa mensagem foi enviada pela pagina de Contato www.wayback.com.br/nacional<br></font>";

                if (smtpmailer($para, get('email'), get('name'), $assunto, $mensagem)) {
                    $_SESSION['json'] = json_encode(array(
                        'error' => false,
                        'msg' => 'Contato enviado com sucesso, obrigado!',
                        'data' => $_POST,
                        'goal' => '/goal/internacional/'. get('tipo')
                    ));
                    back();
                } else {
                    $_SESSION['json'] = json_encode(array(
                        'error' => true,
                        'msg' => 'Não foi possível receber seu contato, tente novamente, caso o erro persista, tente novamente mais tarde.',
                        'data' => $_POST
                    ));
                    back();
                }

            } else {
                back();
            }
        }
    }
}

?>

<!DOCTYPE html>
<!-- xLander - Premium Flexible Landing Page Template design by DSA79 (http://www.dsathemes.com) -->
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <title>Way Back - Cobrança Corporativa Internacional</title>


    <meta name="keywords"
          content="way back, mooca, cobranca, cobrança, cobranca corporativa, cobrança corporativa, recuperacao de credito, recuperação de crédito, recuperacao de creditos, recuperação de créditos, terceirizacao de negocios, terceirização de negócios, bpo, empresa de cobranca, empresa de cobrança, internacional, cobranca internaciona, cobrança internacional, recuperacao de credito internacional, recuperação de crédito internacional, recuperacao de creditos internacional, recuperação de créditos internacional,"/>

    <meta name="author" content="Thiago Jambor">
    <meta name="robots" content="index"/>

    <meta name="description"
          content="Somos especialistas em Cobrança Corporativa Nacional, Internacional e Terceirização de Processos de Negócios. Fale Conosco: 11 2148-9100 | international@wayback.com.br"/>


    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="http://www.wayback.com.br">
    <meta property="og:title" content="Way Back - Cobrança Corporativa e Terceirização de Negócios">
    <meta property="og:site_name" content="Way Back">
    <meta property="og:description" content="Somos especialistas em Cobrança Corporativa Nacional, Internacional e Terceirização de Processos de Negócios. Fale Conosco: 11 2148-9100 | contato@wayback.com.br">

    <meta property="og:image" content="http://www.wayback.com.br/images/wayback-og.png">

    <!-- Libs CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">

    <!-- On Scroll Animations -->
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Template CSS -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/blue.css" rel="stylesheet">


    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">



    <link rel="apple-touch-icon" sizes="57x57" href="../wayback-icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../wayback-icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../wayback-icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../wayback-icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../wayback-icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../wayback-icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../wayback-icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../wayback-icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../wayback-icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../wayback-icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../wayback-icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../wayback-icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../wayback-icon/favicon-16x16.png">
    <link rel="manifest" href="../wayback-icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../wayback-icon/ms-icon-144x144.png">



    <!-- Google Fonts -->
    <link
        href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,800italic,800,700italic,700,600italic,600,400italic,300'
        rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../dist/sweetalert.css">
    <script language="javascript">
        var win = null;
        function NovaJanela(pagina, nome, w, h, scroll) {
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',resizable'
            win = window.open(pagina, nome, settings);
        }
    </script>


    <link rel="stylesheet" href="../css/custom.css" />


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102512770-1', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PW84RV8');</script>
    <!-- End Google Tag Manager -->


</head>


<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PW84RV8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- HEADER
============================================= -->
<header id="header">
    <div class="navbar navbar-fixed-top">
        <div class="container">


            <!-- Navigation Bar -->
            <div class="navbar-header">

                <!-- Responsive Menu Button -->
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse"
                        data-target="#navigation-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Logo Image -->
                <a class="navbar-brand" href="#"><img src="img/logo.png" alt="Way Back - Logo" role="banner"></a>

            </div>    <!-- End Navigation Bar -->


            <!-- Navigation Menu -->
            <nav id="navigation-menu" class="collapse navbar-collapse" role="navigation">
                <ul class="nav navbar-nav navbar-right">

                    <li><a id="GoToHome" class="selected-nav" href="#intro">Home</a></li>
                    <li><a id="GoToCobranca" href="#cobranca">Quem Somos</a></li>
                    <li><a id="GoToDiferenciais" href="#diferenciais">Diferenciais</a></li>
                    <li><a id="GoToIndicadores" href="#indicadores">Fluxo Operacional</a></li>

                    <li><a id="GoToFaq" href="#faq">FAQ</a></li>

                    <li><a id="GoToContato" href="#contato">Contato</a></li>
                </ul>
            </nav>  <!-- End Navigation Menu -->


        </div>     <!-- End container -->
    </div>      <!-- End navbar fixed top  -->
</header>    <!-- END HEADER -->


<!-- CONTENT WRAPPER
============================================= -->
<div id="content_wrapper">


    <!-- INTRO
    ============================================= -->
    <section id="intro">
        <div class="overlay">
            <div class="container">
                <div id="intro_content" class="row">


                    <!-- INTRO TEXT -->
                    <div class="col-md-7">

                        <div class="intro_text">

                            <br><br><br><br><br><br><br>


                            <h1 class="h4a">Cobrança Corporativa Internacional</h1>

                            <br><br>

                            <p>Global em mais de 145 países.
                            </p>

                        </div>


                    </div>    <!-- END TEXT -->


                    <!-- INTRO REGISTER FORM -->
                    <div id="intro_form" class="col-md-5 form_register text-center">

                        <!-- Register Form -->
                        <form action="" name="contactform" class="row" method="post">

                            <input type="hidden" name="tipo" value="conhecer-nossos-servicos" />

                            <div class="fd_key">
                                <input type="text" name="fd_key" value="" />
                            </div>

                            <h4>Quer conhecer nossos serviços? Entre em Contato</h4>

                            <div id="input_name" class="col-md-12">
                                <input class="form-control" type="text" name="name" placeholder="Nome:">
                            </div>

                            <div id="input_email" class="col-md-12">
                                <input class="form-control" type="text" name="email" placeholder="Email:">
                            </div>

                            <div id="input_phone" class="col-md-12">
                                <input class="form-control phone-mask" type="text" name="telephone" pattern="[\(]\d{2}[\)] (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})" placeholder="Telefone:">
                            </div>


                            <div id="input_phone" class="col-md-12">
                                <textarea name="message" class="form-control" placeholder="Mensagem:"></textarea>
                            </div>

                            <p>Entre em contato com nosso Time Comercial e tire suas dúvidas sobre nossos serviços.</p>

                            <!-- Submit Button -->
                            <div id="form_register_btn" class="text-center">
                                <input class="btn btn-theme" type="submit" value="Enviar Mensagem">
                            </div>

                        </form>    <!-- End Register Form -->

                    </div>    <!-- END INTRO REGISTER FORM -->


                </div>     <!-- End Intro Content -->
            </div>    <!-- End container -->
        </div>       <!-- End overlay-->
    </section>    <!-- END INTRO -->


    <!-- ABOUT
    ============================================= -->
    <section id="cobranca">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Cobrança Corporativa Internacional</h3>
                    <h2>Way Back</h2>
                </div>
            </div>


            <!-- TABS HOLDER -->
            <div id="tabs-holder" class="row">


                <!-- TABS -->
                <div class="col-md-7">


                    <!-- Nav Tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active animated" data-animation="bounceIn" data-animation-delay="300"><a
                                href="#tab_1" role="tab" data-toggle="tab">Quem Somos</a></li>
                        <li class=" animated" data-animation="bounceIn" data-animation-delay="400"><a href="#tab_2"
                                                                                                      role="tab"
                                                                                                      data-toggle="tab">Nossos
                                Serviços</a></li>
                    </ul>


                    <!-- Tab Panes -->
                    <div class="tab-content">


                        <!-- TAB #1 -->
                        <div id="tab_1" class="tab-pane active">

                            <!-- Tab #1 Description -->
                            <p>A Way Back Collections | BPO, desde 1991, é especialista em Cobrança Corporativa
                                Nacional, Internacional e na melhoria de processos dentro do conceito de BPO (Business
                                Process Outsourcing).

                            </p><br>
                            <p>Estamos sempre antenados em inovações e melhores práticas de mercado, com o compromisso
                                único de proporcionar o encantamento de nossos clientes e parceiros. </p>

                            <br>
                            <p>Somos Membro <span style="background-color: #bfe9f7">acionista exclusivo no Brasil</span>
                                da TCM Group Global Debt Collection (maior conglomerado de Crédito & Cobrança do mundo)
                                e também filiada ao Instituto GEOC, ACA, IACC e LatinCob.</p>

                            <!-- Tab #2 Buttons -->
                            <a href="http://www.wayback.com.br" target="_blank" class="btn btn-theme animated"
                               data-animation="bounceIn" data-animation-delay="300">Visite nosso Site Institucional</a>


                        </div>    <!-- END TAB #1 -->


                        <!-- TAB #2 -->
                        <div id="tab_2" class="tab-pane">

                            <!-- Tab #2 Description -->
                            <p>A Unidade de Negócios Internacionais da Way Back presta sólida <span
                                    style="background-color: #bfe9f7">assessoria de cobrança</span> para exportadores e
                                prestadores de serviços brasileiros nos cinco continentes em mais de 145 países.</p>

                            <br>

                            <p>
                                Acionamos o inadimplente através de nossa rede de correspondentes diretamente em seu
                                país de origem, quebrando barreiras geográficas, culturais e legais de forma segura,
                                transparente, ética e profissional.

                                <br><br>
                                Somos os maiores interessados em seu sucesso nas cobranças amigáveis: <span
                                    style="background-color: #bfe9f7">“NO WIN NO FEE” (Sem sucesso, sem taxa)</span>.
                                <br><br>
                                Conte conosco para ajudá-los a incrementar os resultados da sua empresa no crescimento
                                exponencial de seus negócios internacionais de forma segura e sustentável.


                            </p>


                            <!-- Tab #2 Buttons -->
                            <a href="#indicadores" class="btn btn-theme animated" data-animation="bounceIn"
                               data-animation-delay="300">Quer entender melhor? Conheça nosso Fluxo Operacional</a>


                        </div>    <!-- END TAB #2 -->


                    </div>    <!-- End Tab Panes -->


                </div>    <!-- END TABS -->


                <!-- TABS HOLDER IMAGE -->
                <div class="col-md-5 text-center animated" data-animation="fadeInRight" data-animation-delay="600">
                    <img class="img-responsive" src="img/thumbs/about-image.jpg" alt="Quem Somos | Nossos Serviços - Way Back" />
                </div>


            </div>    <!-- END TABS HOLDER -->


        </div>      <!-- End container -->
    </section>    <!-- END ABOUT -->


    <!-- PROMO LINE
============================================= -->
    <div id="promo_line">
        <div class="container">
            <div class="row">


                <!-- PROMO LINE CONTENT -->
                <div class="col-md-12 text-center">

                    <h2>Soluções Inteligentes</h2>
                    <br>
                    <p>Conheça mais sobre nossas diversas Soluções Inteligentes, além, da Cobrança Corporativa
                        Internacional </p>
                    <br><br>
                    <a href="http://www.wayback.com.br/nacional" target="_blank" class="btn btn-theme">Cobrança
                        Corporativa Nacional</a>
                    <a href="http://www.wayback.com.br/bpo" target="_blank" class="btn btn-theme2">Terceirização de
                        Processos de Negócios</a>

                </div>


            </div>     <!-- End row -->
        </div>      <!-- End container -->
    </div>        <!-- END PROMO LINE -->


    <!-- SERVICES
    ============================================= -->
    <section id="diferenciais">
        <div class="container">


            <!-- SERVICE IMAGE -->
            <div class="row">

                <div class="col-sm-12 titlebar">
                    <h3>Quais são os</h3>
                    <h2>nossos Diferenciais?</h2>
                </div>


                <div id="services_image" class="col-md-12 text-center animated" data-animation="fadeInUp"
                     data-animation-delay="300">
                    <img class="img-responsive" src="img/thumbs/features-image.png" alt="Nossos diferenciais - Way Back"/>
                </div>
            </div>


            <br>
            <!-- SERVICE BUTTONS -->
            <div class="row">
                <div id="service-buttons" class="col-md-12 text-center">
                    <a href="#contact" class="btn btn-theme animated" data-animation="bounceIn"
                       data-animation-delay="600">Dúvidas? Entre em contato com nosso Time Comercial</a>

                </div>
            </div>


        </div>        <!-- End container -->
    </section>    <!-- END SERVICES -->


    <!-- SERVICES
   ============================================= -->
    <section id="indicadores">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Fluxo Operacional de Cobrança nos países dos inadimplentes</h3>
                    <h2>Como funciona?</h2>
                </div>

                <div id="services_image" class="col-md-12 text-center animated" data-animation="fadeInUp"
                     data-animation-delay="300">
                    <img class="img-responsive" src="img/thumbs/features-image2.png" alt="Como funciona - Way Back"/>
                </div>
            </div>


        </div>        <!-- End container -->
    </section>    <!-- END SERVICES -->


    <!-- ABOUT-1
    ============================================= -->
    <section id="faq">
        <div class="container">


            <!-- ACCORDION HOLDER -->
            <div id="accordion-holder" class="row">


                <!-- ACCORDION HOLDER IMAGE -->
                <div class="col-md-6 text-center animated" data-animation="fadeInLeft" data-animation-delay="600">
                    <img class="img-responsive" src="img/thumbs/tablet.png" alt="Orientações gerais - Way Back"/>
                </div>


                <!--  ACCORDION -->
                <div class="col-md-6">
                    <div class="panel-group" id="accordion">


                        <!-- PANEL #1 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="300">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h5 class="panel-title"><a data-toggle="collapse" class="panel-toggle active"
                                                           data-parent="#accordion" href="#collapseOne"><i
                                            class="fa fa-check"></i> Como será feita a abordagem ao meu cliente
                                        internacional? </a></h5>
                            </div>

                            <!-- Panel Content -->
                            <div id="collapseOne" class="panel-collapse in collapse">
                                <div class="panel-body">
                                    A cobrança dos recebíveis é realizada no país do seu cliente por conhecedores
                                    nativos especialistas em Credit & Collection, respeitando o relacionamento comercial
                                    existente entre as partes, a cultura e a legislação.
                                    <br><br>
                                    Nosso escritório local irá representar sua empresa de maneira transparente e
                                    profissional com o objetivo de trazer os melhores resultados. Sabemos o quanto o seu
                                    cliente é importante e iremos usar a nossa expertise para negociar sua dívida de
                                    maneira estratégica.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #1 -->


                        <!-- PANEL #2 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="500">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseTwo"><i
                                            class="fa fa-check"></i> Como saber sobre o andamento das negociações? </a>
                                </h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Nosso escritório localizado em São Paulo é responsável pela gestão de todos os
                                    processos trabalhados no exterior. Enviamos aos nossos clientes o status dos
                                    processos já traduzidos para o português periodicamente.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #2 -->


                        <!-- PANEL #3 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="700">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseThree"><i
                                            class="fa fa-check"></i> Quanto custa? </a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Cada processo é analisado individualmente, considerando pontos importantes como a
                                    situação política do país em questão, a condição econômico-financeira, a idade da
                                    dívida, histórico de disputa comercial e valores.
                                    <br><br>
                                    A avaliação dos pontos é elaborada em conjunto com nosso escritório local para a
                                    formação do preço.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #3 -->


                        <!-- PANEL #4 -->
                        <div class="panel panel-default animated" data-animation="fadeInRight"
                             data-animation-delay="900">

                            <!-- Panel Title -->
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" class="panel-toggle"
                                                           data-parent="#accordion" href="#collapseFour"><i
                                            class="fa fa-check"></i> O que acontece se tivermos sucesso na recuperação
                                        do seu processo? Como receberei meu dinheiro? </a></h4>
                            </div>

                            <!-- Panel Title -->
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Nossa política é clara e objetiva quando lidamos com os recebíveis de nossos
                                    clientes.
                                    <br><br>
                                    A Way Back estimula seu cliente a realizar o pagamento diretamente para sua empresa.
                                    Enviamos apenas a nota fiscal referente à comissão após a confirmação do dinheiro na
                                    sua conta.
                                </div>
                            </div>

                        </div>    <!-- END PANEL #4 -->


                    </div>
                </div>    <!--  END ACCORDION -->


            </div>    <!-- END ACCORDION HOLDER -->


        </div>      <!-- End container -->
    </section>    <!-- END ABOUT-1 -->


    <!-- TESTIMONIALS
			============================================= -->
    <div id="testimonials">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3>Mais de <span class="clients-counter">300</span> Clientes satisfeitos<br>com o nosso trabalho!
                    </h3>
                </div>
            </div>


            <!-- TESTIMONIALS HOLDER -->
            <div class="row">


                <!-- TESTIMONIAL #1 -->
                <div class="col-sm-6">
                    <div class="testimonial text-center animated" data-animation="fadeInUp" data-animation-delay="500">

                        <!-- Testimonial Message -->
                        <div class="testi-content">
                            <p>"A Way Back foi escolhida a dedo. Conhecida desde minha gestão na empresa LEF PISOS E
                                REVESTIMENTOS LTDA, ela tem mostrado alta performance, alto profissionalismo e um
                                controle de informações sólidos e coerentes para seus clientes. É baseada nessa
                                vivência, que a Way Back foi escolhida novamente, para fazer a gestão de nossas
                                Cobranças Internacionais e tem sido um êxito nos mercados em que acordamos a sua
                                prestação de serviço.
                                <br><br>
                                Tenho realmente satisfação em compartilhar e aprender com essa empresa todos os dias.
                                Parabéns a todos os profissionais que se dedicam a seus clientes com um entusiasmo
                                ilimitado."
                            </p>
                            <div class="testi-arrow"></div>
                        </div>


                        <!-- Testimonial Message Author -->
                        <div class="testi-author">
                            <h4>Juliana Fávaro Polastri</h4>
                            <span>Ceral Pisos</span>
                        </div>

                    </div>
                </div>    <!-- END TESTIMONIAL #1 -->


                <!-- TESTIMONIAL #2 -->
                <div class="col-sm-6">
                    <div class="testimonial text-center animated" data-animation="fadeInUp" data-animation-delay="600">

                        <!-- Testimonial Message -->
                        <div class="testi-content">
                            <p>"I contracted Way Back to recover an important and difficult debt in Japan. I was not
                                optimistic. I had left Japan and returned to the USA. The borrower had ceased responding
                                to any contact after making a partial repayment.
                                <br><br>
                                However, Way Back was able to make contact with the borrower and despite evasive tactics
                                recovered all of the money that was still outstanding.
                                <br><br>
                                I recommend Way Back with no reservations.” </p>
                            <div class="testi-arrow"></div>
                        </div>


                        <!-- Testimonial Message Author -->
                        <div class="testi-author">
                            <h4>David Dwyer</h4>
                            <span>EUA</span>
                        </div>
                    </div>
                </div>    <!-- END TESTIMONIAL #2 -->


            </div>    <!-- END TESTIMONIALS HOLDER -->


        </div>     <!-- End container -->
    </div>       <!-- END TESTIMONIALS -->


    <!-- CONTACT-INFO
    ============================================= -->
    <section id="contato">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Fale Conosco</h3>
                    <h2>Entre em contato com nosso Time Comercial</h2>
                </div>
            </div>


            <div class="row">


                <!-- Office Location -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-map-marker"></i>
                    <h4 class="contact_title">Localização</h4>
                    <p class="contact_description">Av. Paes de Barros, 1162 - Mooca<br>03114-000 - São Paulo/SP</p>
                </div>


                <!-- Call Us -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-phone"></i>
                    <h4 class="contact_title">Telefone</h4>
                    <p class="contact_description"> +55 11 2148-9100<br>+1 (305) 423 7132</p>
                </div>


                <!-- Email Address -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-envelope-o"></i>
                    <h4 class="contact_title">Email</h4>
                    <a href="mailto:comercial@wayback.com.br">international@wayback.com.br</a>
                </div>


                <!-- Working Hours -->
                <div class="col-xs-6 col-sm-6 col-md-3 contact-info text-center triggerAnimation animated"
                     data-animate="bounceIn">
                    <i class="fa fa-clock-o"></i>
                    <h4 class="contact_title">Atendimento</h4>
                    <p class="contact_description">Seg. à Qui. - 08h às 18h<br>Sex. - 08h às 17h</p>
                </div>


            </div>    <!-- End row -->


        </div>             <!-- End container -->
    </section>       <!-- END CONTACT-INFO -->


    <!-- CONTACTS
     ============================================= -->
    <section id="contact">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-sm-12 titlebar">
                    <h3>Dúvidas ou Sugestões?</h3>
                    <h2>Deixe sua mensagem que retornaremos o contato</h2>
                </div>
            </div>


            <!-- CONTACT FORM -->
            <div class="row">
                <div class="col-sm-12">

                    <form id="contact-form" action="" name="contactform" class="row" method="post">

                        <input type="hidden" name="tipo" value="duvidas-e-sugestoes" />

                        <div class="fd_key">
                            <input type="text" name="fd_key" value="" />
                        </div>

                        <div id="input_name" class="col-md-6">
                            <input type="text" name="name" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Nome:">
                        </div>

                        <div id="input_email" class="col-md-6">
                            <input type="text" name="empresa" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Empresa:">
                        </div>


                        <div id="input_name" class="col-md-6">
                            <input type="text" name="telephone" pattern="[\(]\d{2}[\)] (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})" class="form-control text-white phone-mask" placeholder="Telefone:">
                        </div>

                        <div id="input_email" class="col-md-6">
                            <input type="text" name="email" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Email:">
                        </div>


                        <div id="input_subject" class="col-md-12">
                            <input type="text" name="assunto" class="form-control text-white triggerAnimation animated"
                                   data-animate="bounceIn" placeholder="Assunto:">
                        </div>

                        <div id="input_message" class="col-md-12">
                            <textarea class="form-control triggerAnimation text-white animated" data-animate="bounceIn"
                                      name="message" rows="6" placeholder="Mensagem:"></textarea>
                        </div>

                        <!-- Submit Button -->
                        <div id="form_btn" class="col-md-12">
                            <div class="text-center">
                                <input type="submit" value="ENVIAR MENSAGEM" class="btn btn-theme triggerAnimation animated" data-animate="bounceIn">
                            </div>
                        </div>

                    </form>

                </div>
            </div>       <!-- END CONTACT FORM -->


        </div>       <!-- End container -->
    </section>     <!-- END CONTACTS -->


    <!-- FOOTER
    ============================================= -->
    <footer id="footer">
        <div class="container">


            <!-- FOOTER COPYRIGHT -->
            <div class="row">
                <div id="footer_copyright" class="col-sm-12 text-center">
                    <p>Copyright Way Back © 2017. All Rights Reserved.</p>
                </div>
            </div>


            <!-- FOOTER SOCIALS -->
            <div class="row">

                <div id="footer_socials" class="col-sm-12 text-center">

                    <div id="contact_icons">
                        <ul class="contact-socials clearfix">
                            <li><a class="foo_social ico-facebook" href="https://www.facebook.com/WayBack.BPO"
                                   target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="foo_social ico-linkedin"
                                   href="https://www.linkedin.com/company/way-back-servi-os-de-cobran-a-ltda"
                                   target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a class="foo_social ico-twitter" href="https://twitter.com/waybackbr"
                                   target="_blank"><i class="fa fa-twitter"></i></a></li>

                            <li><a class="foo_social ico-youtube" href="https://www.youtube.com/user/WayBackBR"
                                   target="_blank"><i class="fa fa-youtube"></i></a></li>

                            <!--
                                <li><a class="foo_social ico-dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a class="foo_social ico-pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="foo_social ico-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="foo_social ico-digg" href="#"><i class="fa fa-digg"></i></a></li>
                                <li><a class="foo_social ico-deviantart" href="#"><i class="fa fa-deviantart"></i></a></li>
                                <li><a class="foo_social ico-envelope" href="#"><i class="fa fa-envelope-square"></i></a></li>
                                <li><a class="foo_social ico-delicious" href="#"><i class="fa fa-delicious"></i></a></li>
                                <li><a class="foo_social ico-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="foo_social ico-dropbox" href="#"><i class="fa fa-dropbox"></i></a></li>
                                <li><a class="foo_social ico-skype" href="#"><i class="fa fa-skype"></i></a></li>
                                <li><a class="foo_social ico-youtube" href="#"><i class="fa fa-youtube"></i></a></li>
                                <li><a class="foo_social ico-tumblr" href="#"><i class="fa fa-tumblr"></i></a></li>
                                <li><a class="foo_social ico-vimeo" href="#"><i class="fa fa-vimeo-square"></i></a></li>
                                <li><a class="foo_social ico-flickr" href="#"><i class="fa fa-flickr"></i></a></li>
                                <li><a class="foo_social ico-github" href="#"><i class="fa fa-github-alt"></i></a></li>
                                <li><a class="foo_social ico-renren" href="#"><i class="fa fa-renren"></i></a></li>
                                <li><a class="foo_social ico-vk" href="#"><i class="fa fa-vk"></i></a></li>
                                <li><a class="foo_social ico-xing" href="#"><i class="fa fa-xing"></i></a></li>
                                <li><a class="foo_social ico-weibo" href="#"><i class="fa fa-weibo"></i></a></li>
                                <li><a class="foo_social ico-rss" href="#"><i class="fa fa-rss"></i></a></li>
                            -->

                        </ul>
                    </div>

                </div>

            </div>    <!-- END FOOTER SOCIALS -->


        </div>      <!-- End container -->
    </footer>    <!-- END FOOTER -->


</div>    <!-- END CONTENT WRAPPER -->


<!-- EXTERNAL SCRIPTS
============================================= -->
<script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/modernizr.custom.js" type="text/javascript"></script>
<script src="js/jquery.mask.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.js" type="text/javascript"></script>
<script src="js/retina.js" type="text/javascript"></script>
<script src="js/jquery.stellar.min.js" type="text/javascript"></script>
<script defer src="js/jquery.flexslider.js" type="text/javascript"></script>
<script src="js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script defer src="js/count-to.js"></script>
<script defer src="js/jquery.appear.js"></script>
<script src="js/jquery.mixitup.js" type="text/javascript"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script src="js/jquery.easypiechart.min.js"></script>
<script defer src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/waypoints.min.js" type="text/javascript"></script>
<script src="../cobranca-corporativa-nacional/js/custom.js" type="text/javascript"></script>



<script src="../dist/sweetalert-dev.js"></script>
<script language="javascript">
    var win = null;
    function NovaJanela(pagina, nome, w, h, scroll) {
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',resizable'
        win = window.open(pagina, nome, settings);
    }

    function JSalert($title, $text, $status) {
        swal($title, $text, $status);
    }
</script>

<script type="text/javascript">
    <?php
    if (session_has('json')) {
        $json = json_decode(session('json'));
        if ($json->error) {
            echo 'JSalert("Ops", "'.$json->msg.'", "error");';
        } else {
            echo 'JSalert("Contato", "'.$json->msg.'", "success");';
            echo 'ga("send", "pageview", "'.$json->goal.'");';
        }
        session_destroy();
    }
    ?>
</script>



<!-- Google Map -->
<!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
<!--<script src="js/jquery.gmap3.min.js"></script>-->

<!-- Google Map Init-->
<!--<script type="text/javascript">-->
<!--    jQuery(function ($) {-->
<!--        $('#map_canvas').gmap3({-->
<!--            marker: {-->
<!--                address: '-23.56472045,-46.59618393',-->
<!--                options: {icon: "img/icons/map-marker.png"}-->
<!--            },-->
<!--            map: {-->
<!--                options: {-->
<!--                    zoom: 16,-->
<!--                    scrollwheel: false,-->
<!--                    streetViewControl: true,-->
<!--                    styles: [{-->
<!--                        featureType: "all",-->
<!--                        stylers: [-->
<!--                            {saturation: -100}-->
<!--                        ]-->
<!--                    }, {-->
<!--                        featureType: "road.arterial",-->
<!--                        elementType: "geometry",-->
<!--                        stylers: [-->
<!--                            {hue: "#000000"},-->
<!--                            {saturation: -100}-->
<!--                        ]-->
<!--                    }-->
<!--                    ],-->
<!--                }-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->


<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->


<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->

<script type="text/javascript">
    var _fdq = _fdq || [];
    _fdq._setAccount = 'FD_0011';

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.fatordigital.com.br/metriks.min.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

<script type="text/javascript" src="https://s3-sa-east-1.amazonaws.com/phonetrack-static/59c33016884a62116be975a9bb8257e3.js" id="script-pht-phone" data-cookiedays="5"></script>


</body>

</html>