<?php
# Envio de e-mail, função usada pelo cliente
function smtpmailer($para, $de, $de_nome, $assunto, $corpo)
{
    $mail = new PHPMailer();
    $mail->IsSMTP();    // Ativar SMTP
    $mail->SMTPDebug = 0;   // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
    $mail->SMTPAuth = true;   // Autenticação ativada
    $mail->SMTPSecure = 'ssl';  // SSL REQUERIDO pelo GMail
    $mail->Host = 'smtp.gmail.com'; // SMTP utilizado
    $mail->Port = 465;      // A porta 587 deverá estar aberta em seu servidor ou a 465
    $mail->IsHTML(true);
    $mail->Username = GUSER;
    $mail->Password = GPWD;
    $mail->AddReplyTo($de, $de_nome);
    $mail->addBCC('andrew.rodrigues@fatordigital.com.br');
    //$mail->SetFrom($de, $de_nome);
    $mail->From = $de;
    $mail->FromName = $de_nome;
    $mail->CharSet = 'UTF-8';
    $mail->Subject = $assunto;
    $mail->Body = $corpo;
    $mail->AddAddress($para);
    if (!$mail->Send()) {
        return false;
    } else {
        return true;
    }
}

# Anti-Spam Fator Digital
function fator_digital_key()
{
    $post = $_POST;
    if (!isset($post['fd_key']) || $post['fd_key'] != '')
        return false;
    return true;
}

# Verifica se o campo existe e não está vazio
function has_empty ($key) {
    return isset($_POST[$key]) && empty($_POST[$key]);
}

# Pega o valor do parâmetro post
function get($key) {
    return !has_empty($key) ? $_POST[$key] : '';
}

# Retorna para a página anterior
function back () {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
}

# Redireciona para o link passado
function redirect ($link) {
    header('Location: ' . $link);
    exit;
}

# Se existir a sessão $key
function session_has ($key) {
    return isset($_SESSION[$key]);
}

# Pega a sessão $key
function session ($key) {
    return $_SESSION[$key];
}

function swal($title, $message, $status) {
    return 'swal("'.$title.'", "'.$message.'", "'.$status.'")';
}